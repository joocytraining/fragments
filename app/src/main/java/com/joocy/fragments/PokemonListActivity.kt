package com.joocy.fragments

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.joocy.fragments.PokemonRepository.Pokemon
import kotlinx.android.synthetic.main.activity_pokemon_list.*
import kotlinx.android.synthetic.main.pokemon_list.*

class PokemonListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokemon_list)

        setSupportActionBar(toolbar)
        toolbar.title = title

        val selectionHandler = when (pokemon_detail_container) {
            null -> SingleViewPokemonClickHandler(this)
            else -> MultiViewPokemonClickHandler(this)
        }
        val monsters = PokemonRepository.monsters.also{it.sortBy(Pokemon::name)}
        pokemon_list.adapter = PokemonListAdapter(monsters, selectionHandler)
    }

}
