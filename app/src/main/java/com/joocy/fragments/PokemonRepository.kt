package com.joocy.fragments

import java.util.ArrayList
import java.util.HashMap

object PokemonRepository {

    val monsters: MutableList<Pokemon> = ArrayList()

    val monsterMap: MutableMap<String, Pokemon> = HashMap()

    init {
        val venusaur = Pokemon(
            name = "Venusaur",
            primaryType = Type.GRASS,
            secondaryType = Type.POISON,
            hp = 80,
            attack = 82,
            defense = 83
        )
        addMonster(venusaur)
        val ivysaur = Pokemon(
            name = "Ivysaur",
            primaryType = Type.GRASS,
            secondaryType = Type.POISON,
            hp = 60,
            attack = 62,
            defense = 63,
            evolvesInto = venusaur
        )
        addMonster(ivysaur)
        val bulbasaur = Pokemon(
            name = "Bulbasaur",
            primaryType = Type.GRASS,
            secondaryType = Type.POISON,
            hp = 45,
            attack = 49,
            defense = 49,
            evolvesInto = ivysaur
        )
        addMonster(bulbasaur)
        val raichu = Pokemon(
            name = "Raichu",
            primaryType = Type.ELECTRIC,
            hp = 60,
            attack = 90,
            defense = 55
        )
        addMonster(raichu)
        val pikachu = Pokemon(
            name = "Pikachu",
            primaryType = Type.ELECTRIC,
            hp = 35,
            attack = 55,
            defense = 40,
            evolvesInto = raichu
        )
        addMonster(pikachu)
        val ninetales = Pokemon(
            name ="Ninetales",
            primaryType = Type.FIRE,
            hp = 73,
            attack = 76,
            defense = 75
        )
        addMonster(ninetales)
        val vulpix = Pokemon(
            name = "Vulpix",
            primaryType = Type.FIRE,
            hp = 36,
            attack = 41,
            defense = 40,
            evolvesInto = ninetales
        )
        addMonster(vulpix)
    }

    private fun addMonster(monster: Pokemon) {
        monsters.add(monster)
        monsterMap.put(monster.name, monster)
    }

    data class Pokemon(
        val name: String,
        val primaryType: Type,
        val secondaryType: Type? = null,
        val hp: Long,
        val attack: Long,
        val defense: Long,
        val evolvesInto: Pokemon? = null) {
        override fun toString(): String = name
    }

    enum class Type(val color: Int) {
        NORMAL(R.color.normal),
        BUG(R.color.bug),
        ELECTRIC(R.color.electric),
        GRASS(R.color.grass),
        GROUND(R.color.ground),
        POISON(R.color.poison),
        FIRE(R.color.fire),
        WATER(R.color.water),
        FLYING(R.color.flying),
        FAIRY(R.color.fairy),
        ROCK(R.color.rock),
        FIGHTING(R.color.fighting);

        val displayName
            get () = this.name.toLowerCase().capitalize()
    }
}

