package com.joocy.fragments

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class PokemonDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pokemon_detail)

        if (savedInstanceState == null) {
            val monsterName = intent.getStringExtra(PokemonDetailFragment.ARG_MONSTER_NAME)
            PokemonDetailFragment.display(monsterName, R.id.pokemon_detail_container, supportFragmentManager)
        }
    }

}
